(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(use-package auto-package-update
  :ensure t
  :config
  (setq auto-package-update-delete-old-versions t)
  (setq auto-package-update-hide-results t)
  (auto-package-update-maybe))

(use-package evil
  :ensure t
  :init
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  :config
  (evil-mode 1))

(use-package undo-tree
  :ensure t)

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

(use-package evil-surround
  :after evil
  :ensure t
  :config
  (global-evil-surround-mode 1))

(use-package evil-numbers
  :after evil
  :ensure t
  :bind (("C-a" . evil-numbers/inc-at-pt)
	 ("C-x C-x" . evil-numbers/dec-at-pt)))

(use-package flycheck
  :ensure t
  :config
  (global-flycheck-mode 1))

(use-package evil-org
  :ensure t
  :after org
  :hook ((org-mode . evil-org-mode)
	 (evil-org-mode . (lambda () (evil-org-set-key-theme))))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package magit
  :ensure t)

(use-package git-modes
  :ensure t)

(use-package projectile
  :ensure t
  :config
  (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1))

(use-package helm
  :ensure t
  :config
  (helm-mode 1))

(use-package helm-projectile
  :ensure t
  :after (helm projectile)
  :config
  (require 'helm-projectile)
  (helm-projectile-on))

(use-package helm-rg
  :ensure t
  :after helm)

(use-package keychain-environment
  :ensure t
  :config
  (keychain-refresh-environment))

(use-package base16-theme
  :ensure t
  :demand
  :config
  (load-theme 'base16-monokai t)
  ;; Set the cursor color based on the evil state
  (defvar my/base16-colors base16-monokai-theme-colors)
  (setq evil-emacs-state-cursor   `(,(plist-get my/base16-colors :base0D) box)
	evil-insert-state-cursor  `(,(plist-get my/base16-colors :base0D) bar)
	evil-motion-state-cursor  `(,(plist-get my/base16-colors :base0E) box)
	evil-normal-state-cursor  `(,(plist-get my/base16-colors :base0B) box)
	evil-replace-state-cursor `(,(plist-get my/base16-colors :base08) bar)
	evil-visual-state-cursor  `(,(plist-get my/base16-colors :base09) box)))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
	 ("\\.md\\'" . markdown-mode)
	 ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc"))

(use-package toml-mode
  :ensure t)

(use-package rustic
  :ensure t)

(use-package company
  :ensure t)

(use-package lsp-mode
  :ensure t
  :init (setq lsp-keymap-prefix "s-l")
  :hook (python-mode . lsp)
  :commands lsp)

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

(use-package helm-lsp
  :ensure t
  :commands helm-lsp-workspace-symbol)

(use-package lsp-treemacs
  :ensure t
  :commands lsp-treemacs-errors-list)

(use-package dap-mode
  :ensure t
  :config
  (dap-mode 1)
  (dap-ui-mode 1)
  (dap-tooltip-mode 1))

(use-package treemacs
  :ensure t
  :defer t
  :bind
  (:map global-map
	("M-0"       . treemacs-select-window)
	("C-x t 1"   . treemacs-delete-other-windows)
	("C-x t t"   . treemacs)
	("C-x t B"   . treemacs-bookmark)
	("C-x t C-t" . treemacs-find-file)
	("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-evil
  :after treemacs evil
  :ensure t)

(use-package treemacs-projectile
  :after treemacs projectile
  :ensure t)

(use-package treemacs-icons-dired
  :after treemacs dired
  :ensure t)

(use-package treemacs-magit
  :after treemacs magit
  :ensure t)

(use-package elcord
  :ensure t)

(use-package typescript-mode
  :ensure t)

(use-package web-mode
  :ensure t
  :mode (("\\.tsx\\'" . web-mode)
	 ("\\.eco\\'" . web-mode))
  :config
  (setq web-mode-engines-alist
	'(("erb" . "\\.eco\\'"))))

(use-package tide
  :ensure t
  :after (typescript-mode company web-mode)
  :hook ((typescript-mode . tide-setup)
	 (typescript-mode . tide-hl-identifier-mode)
	 (before-save . tide-format-before-save)
	 (web-mode . (lambda ()
		       (when (string-equal "tsx" (file-name-extension buffer-file-name))
			 (tide-setup)
			 (tide-hl-identifier-mode))))))

(use-package tex
  :ensure auctex)

(defun my-display-startup (where)
  t)

(use-package latex-preview-pane
  :after tex
  :ensure t
  :hook ((LaTeX-mode . latex-preview-pane-mode))
  :config
  (advice-add 'lpp/display-startup :override #'my-display-startup))

(use-package ripgrep
  :ensure t)

(use-package pretty-mode
  :ensure t)

(use-package latex-pretty-symbols
  :load-path "lisp/")

(use-package nusmv-mode
  :load-path "/usr/share/nusmv/contrib"
  :mode (("\\.smv\\'" . nusmv-mode)))

(use-package general
  :after evil
  :ensure t
  :config
  (general-evil-setup t)
  (imap "j"
	(general-key-dispatch 'self-insert-command
	  :timeout 0.25
	  "k" 'evil-normal-state))
  (general-swap-key nil 'motion
    "0" "^"))

(use-package elixir-mode
  :ensure t)

(use-package alchemist
  :ensure t)

(use-package yaml-mode
  :ensure t
  :mode (("\\.yaml'" . yaml-mode)))

(use-package docker
  :ensure t)

(use-package dockerfile-mode
  :ensure t
  :mode (("Dockerfile'" . dockerfile-mode)))

(use-package coffee-mode
  :ensure t)

(use-package js2-mode
  :ensure t
  :mode (("\\.js\\'" . js2-mode)))

(use-package ein
  :ensure t)

(use-package org-download
  :ensure t)

(use-package exec-path-from-shell
  :ensure t
  :config
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)))

(use-package ligature
  :ensure t
  :config
  ;; Enable the www ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))

  ;; Enable ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\" "{-" "::"
                                       ":::" ":=" "!!" "!=" "!==" "-}" "----" "-->" "->" "->>"
                                       "-<" "-<<" "-~" "#{" "#[" "##" "###" "####" "#(" "#?" "#_"
                                       "#_(" ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*" "/**"
                                       "/=" "/==" "/>" "//" "///" "&&" "||" "||=" "|=" "|>" "^=" "$>"
                                       "++" "+++" "+>" "=:=" "==" "===" "==>" "=>" "=>>" "<="
                                       "=<<" "=/=" ">-" ">=" ">=>" ">>" ">>-" ">>=" ">>>" "<*"
                                       "<*>" "<|" "<|>" "<$" "<$>" "<!--" "<-" "<--" "<->" "<+"
                                       "<+>" "<=" "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<"
                                       "<~" "<~~" "</" "</>" "~@" "~-" "~>" "~~" "~~>" "%%"))
  (global-ligature-mode 't))

(require 'em-smart)

(setq default-frame-alist '((font . "FiraCode Nerd Font 12")))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(coffee-tab-width 2)
 '(company-tooltip-align-annotations t)
 '(dired-dwim-target t)
 '(display-line-numbers 'visual)
 '(display-line-numbers-type 'visual)
 '(elcord-mode t nil (elcord))
 '(electric-pair-mode t)
 '(evil-collection-setup-minibuffer t)
 '(evil-ex-substitute-global t)
 '(evil-undo-system 'undo-tree)
 '(evil-want-minibuffer t)
 '(fira-code-mode-disabled-ligatures '("x"))
 '(fira-code-mode-enable-hex-literal nil)
 '(global-company-mode t)
 '(global-prettify-symbols-mode t)
 '(global-undo-tree-mode t)
 '(helm-completion-style 'helm-fuzzy)
 '(helm-minibuffer-history-key "M-p")
 '(indent-tabs-mode nil)
 '(initial-buffer-choice t)
 '(js-indent-level 2)
 '(latex-preview-pane-multifile-mode 'auctex)
 '(lsp-prefer-flymake nil)
 '(menu-bar-mode nil)
 '(org-startup-truncated nil)
 '(package-selected-packages
   '(ligature exec-path-from-shell org-download helm-rg helm-projectile js2-mode coffee-mode dockerfile-mode docker yaml-mode evil-numbers rustic auto-package-update general dap-python dap-mode ripgrep latex-preview-pane auctex pig-mode evil-org web-mode tide typescript-mode elcord company evil-magit projectile nlinum-relative keychain-environment evil-collection use-package magit evil-surround base16-theme))
 '(projectile-completion-system 'helm)
 '(projectile-switch-project-action 'helm-projectile-find-file)
 '(revert-without-query '(".+\\.pdf"))
 '(rust-indent-method-chain t)
 '(rustic-format-trigger 'on-save)
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil)
 '(treemacs-icons-dired-mode t nil (treemacs-icons-dired))
 '(web-mode-code-indent-offset 2)
 '(web-mode-markup-indent-offset 2)
 '(yas-global-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
